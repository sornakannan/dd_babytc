package com.dollardays.pages;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.dollardays.commons.Base64;

public class babypage {

	WebDriver driver;

	public babypage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	//@FindBy(xpath = "//*[@id='header-main']/div/div/div[2]/div[1]/div[1]/input")
	@FindBy(xpath="//*[@id=\"sm_menu_ham\"]")
	private WebElement menubtn;
	

	public WebElement getmenubtn() {
		return menubtn;
	}
	
	//@FindBy(xpath = "//*[@id='header-main']/div/div/div[2]/div[1]/div[1]/div/div/button")
	@FindBy(xpath="//*[@id=\"aspnetForm\"]/header/div/div/div/div[1]/div/div[3]/div[2]/ul/li[3]/a")
	private WebElement babymenulink;

	public WebElement getbabymenulink() {
		return babymenulink;
	}
	
	@FindBy(xpath = "//*[@id=\"aspnetForm\"]/header/div/div/div/div[1]/div/div[3]/div[2]/ul/li[3]/ul")
	private WebElement babysubmenupage;

	public WebElement babysubmenupage() {
		return babysubmenupage;
	}
	
	@FindBy(xpath = "//*[@id=\"aspnetForm\"]/header/div/div/div/div[1]/div/div[3]/div[2]/ul/li[3]/ul/li[1]/a")
	private WebElement backbtn;

	public WebElement getbackbtn() {
		return backbtn;
	}
	
	@FindBy(xpath = "//*[@id=\"aspnetForm\"]/header/div/div/div/div[1]/div/div[3]/div[2]/ul/li[3]/ul/li[3]/a")
	private WebElement babygearlnk;
	
	public WebElement getbabygearlnk() {
	
		return babygearlnk;
	}
	
	/*@FindElement(By.cssSelector(#aspnetForm > header > div > div > div > div.col-lg-5.col-md-5.col-sm-6.col-xs-5 > div > div.mobile-menu-toggle > div.sm_menu_outer.active > ul > li.hasChild.active > ul > li:nth-child(3) > a);
	private WebElement babygearlnk;

	public WebElement getbabygearlnk() {
		return babygearlnk;
	}*/
	
	@FindBy(xpath = "//*[@id=\"aspnetForm\"]/header/div/div/div/div[1]/div/div[3]/div[2]/ul/li[3]/ul/li[4]/a")
	private WebElement bathinggroominglnk;

	public WebElement getbathinggroominglnk() {
		return bathinggroominglnk;
	}

	@FindBy(xpath = "//*[@id=\"aspnetForm\"]/header/div/div/div/div[1]/div/div[3]/div[2]/ul/li[3]/ul/li[5]/a")
	private WebElement babyblanketlnk;

	public WebElement getbabyblanketlnk() {
		return babyblanketlnk;
	}
	
	@FindBy(xpath = "//*[@id=\"aspnetForm\"]/header/div/div/div/div[1]/div/div[3]/div[2]/ul/li[3]/ul/li[6]/a")
	private WebElement clothinglnk;
	
	public WebElement getclothinglnk() {
	
		return clothinglnk;
	}
	
	@FindBy(xpath = "//*[@id=\"aspnetForm\"]/header/div/div/div/div[1]/div/div[3]/div[2]/ul/li[3]/ul/li[6]/ul/li[1]/a")
	private WebElement backbtn1;

	public WebElement getbackbtn1() {
		return backbtn1;
	}	

}
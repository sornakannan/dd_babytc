package com.dollardays.testcases;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Hashtable;
import java.util.Map;

import org.junit.Ignore;
import org.testng.annotations.Test;
import org.junit.*;
import com.aventstack.extentreports.Status;
import com.dollardays.commons.Base64;
import com.dollardays.listners.ExtentTestManager;
import com.dollardays.pages.babypage;
import com.dollardays.utilities.DDDataProvider;
import com.dollardays.utilities.JsonReader;
import com.dollardays.utilities.TestUtil;
import com.dollardays.utilities.VideoRecorder_utlity;

public class babytestcase extends BaseTest{

	
    
	@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
    
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)

	public void TC_DD_Baby_01(Hashtable<String, String> datatable) throws Exception{
		//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
		ExtentTestManager.getTest().log(Status.INFO, " verify babymnulink testcase");
		Thread.sleep(1000);
		
		//babypage bp = new babypage(driver);
		//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
		//VideoRecorder_utlity.stopRecord();//End point of video recording
		
		
		//creating object for babypage.java
		
		babypage bp = new babypage(driver);
		
		//tc1 click main --> baby then click baby
		
		bp.getmenubtn().click();
		Thread.sleep(1000);
		bp.getbabymenulink().click();
		Thread.sleep(1000);
		bp.getbackbtn().click();
		Thread.sleep(1000);
		ExtentTestManager.getTest().log(Status.INFO, "TC_01 Passed");
		                         
		
	}
	
	@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
	
		public void TC_DD_Baby_02(Hashtable<String, String> datatable) throws Exception{
		//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
		ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_babygearlnk testcase");
		Thread.sleep(1000);
		
		//babypage bp = new babypage(driver);
		//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
		//VideoRecorder_utlity.stopRecord();//End point of video recording
		
		
		//creating object for babypage.java
		
		babypage bp = new babypage(driver);
		
		//tc1 click main --> baby then click baby
		
		bp.getmenubtn().click();
		Thread.sleep(1000);
		bp.getbabymenulink().click();
		Thread.sleep(1000);
		bp.getbabygearlnk().click();
		Thread.sleep(1000);
		//bp.getbackbtn().click();
		//Thread.sleep(1000);
		ExtentTestManager.getTest().log(Status.INFO, "TC_02 Passed");
		                         
		
	}
	/* @DDDataProvider(datafile = "testdata/testdata.json", runmode = "", sheetName = "", testcaseID = "")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = JsonReader.class)
	public void invokeLogin1(Map<String, String> datatable) throws Exception{
		System.out.println("====>"+ datatable);
		System.out.println("Username====>"+ datatable.get("usernme"));
		
		System.out.println("Password====>"+ Base64.decrypt(datatable.get("password")));
		
	}*/
	
	@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
	@Ignore
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
	public void TC_DD_Baby_03(Hashtable<String, String> datatable) throws Exception{
		//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
		ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_bathing&groominglnk testcase");
		Thread.sleep(1000);
		
		//babypage bp = new babypage(driver);
		//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
		//VideoRecorder_utlity.stopRecord();//End point of video recording
		
		
		//creating object for babypage.java
		
		babypage bp = new babypage(driver);
		
		//tc1 click main --> baby then click baby
		
		bp.getmenubtn().click();
		Thread.sleep(1000);
		bp.getbabymenulink().click();
		Thread.sleep(1000);
		bp.getbathinggroominglnk().click();
		//bp.getbackbtn().click();
		//Thread.sleep(2000);
		ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_03 Passed");
		                         
		
	}
	@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
    @Ignore
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
	
	public void TC_DD_Baby_04(Hashtable<String, String> datatable) throws Exception{
		//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
		ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_baby&blanket testcase");
		Thread.sleep(1000);
		
		//babypage bp = new babypage(driver);
		//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
		//VideoRecorder_utlity.stopRecord();//End point of video recording
		
		
		//creating object for babypage.java
		
		babypage bp = new babypage(driver);
		
		//tc1 click main --> baby then click baby
		
		bp.getmenubtn().click();
		Thread.sleep(1000);
		bp.getbabymenulink().click();
		Thread.sleep(1000);
		bp.getbabyblanketlnk().click();
		//bp.getbackbtn().click();
		//Thread.sleep(2000);
		ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_04 Passed");
		                         
		
	}
	@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
    @Ignore
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
	
	public void TC_DD_Baby_05(Hashtable<String, String> datatable) throws Exception{
		//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
		ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_clothinglnk testcase");
		Thread.sleep(1000);
		
		//babypage bp = new babypage(driver);
		//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
		//VideoRecorder_utlity.stopRecord();//End point of video recording
		
		
		//creating object for babypage.java
		
		babypage bp = new babypage(driver);
		
		//tc1 click main --> baby then click baby
		
		bp.getmenubtn().click();
		Thread.sleep(1000);
		bp.getbabymenulink().click();
		Thread.sleep(1000);
		bp.getclothinglnk().click();
		Thread.sleep(2000);
		bp.getclothingbackbtn().click();
		Thread.sleep(2000);
		bp.getbackbtn().click();
		ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_05 Passed");
		                         
		
	}
	
@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
    @Ignore
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
	
	public void TC_DD_Baby_06(Hashtable<String, String> datatable) throws Exception{
		//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
		ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_diaperinglnk testcase");
		Thread.sleep(1000);
		
		//babypage bp = new babypage(driver);
		//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
		//VideoRecorder_utlity.stopRecord();//End point of video recording
		
		
		//creating object for babypage.java
		
		babypage bp = new babypage(driver);
		
		//tc1 click main --> baby then click baby
		
		bp.getmenubtn().click();
		Thread.sleep(1000);
		bp.getbabymenulink().click();
		Thread.sleep(1000);
		bp.getdiaperinglnk().click();
		Thread.sleep(2000);
		bp.getdiaperingbackbtn().click();
		bp.getbackbtn().click();
		
		ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_06 Passed");
		}


@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
@Ignore
@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)

public void TC_DD_Baby_07(Hashtable<String, String> datatable) throws Exception{
	//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
	ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_feedinglnk testcase");
	Thread.sleep(1000);
	
	//babypage bp = new babypage(driver);
	//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
	//VideoRecorder_utlity.stopRecord();//End point of video recording
	
	
	//creating object for babypage.java
	
	babypage bp = new babypage(driver);
	
	//tc1 click main --> baby then click baby
	
	bp.getmenubtn().click();
	Thread.sleep(1000);
	bp.getbabymenulink().click();
	Thread.sleep(1000);
	bp.getfeedinglnk().click();
	Thread.sleep(2000);
	bp.getfeedingbackbtn().click();
    Thread.sleep(2000);
	bp.getbackbtn().click();
	ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_07 Passed");
	                         
	
}

@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
@Ignore
@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)

public void TC_DD_Baby_08(Hashtable<String, String> datatable) throws Exception{
	//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
	ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_healthsafetylnk testcase");
	Thread.sleep(1000);
	
	//babypage bp = new babypage(driver);
	//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
	//VideoRecorder_utlity.stopRecord();//End point of video recording
	
	
	//creating object for babypage.java
	
	babypage bp = new babypage(driver);
	
	//tc1 click main --> baby then click baby
	
	bp.getmenubtn().click();
	Thread.sleep(1000);
	bp.getbabymenulink().click();
	Thread.sleep(1000);
	bp.gethealthsafetylnk().click();
	Thread.sleep(2000);
	
	ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_08 Passed");
	
}

@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
@Ignore
@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)

public void TC_DD_Baby_09(Hashtable<String, String> datatable) throws Exception{
	//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
	ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_kitsgiftsetlnk testcase");
	Thread.sleep(1000);
	
	//babypage bp = new babypage(driver);
	//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
	//VideoRecorder_utlity.stopRecord();//End point of video recording
	
	
	//creating object for babypage.java
	
	babypage bp = new babypage(driver);
	
	//tc1 click main --> baby then click baby
	
	bp.getmenubtn().click();
	Thread.sleep(1000);
	bp.getbabymenulink().click();
	Thread.sleep(1000);
	bp.getkitsgiftsetslnk().click();
	Thread.sleep(2000);
	
	ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_09 Passed");
	
}

@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
@Ignore
@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)

public void TC_DD_Baby_10(Hashtable<String, String> datatable) throws Exception{
	//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
	ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_shoeslnk testcase");
	Thread.sleep(1000);
	
	//babypage bp = new babypage(driver);
	//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
	//VideoRecorder_utlity.stopRecord();//End point of video recording
	
	
	//creating object for babypage.java
	
	babypage bp = new babypage(driver);
	
	//tc1 click main --> baby then click baby
	
	bp.getmenubtn().click();
	Thread.sleep(1000);
	bp.getbabymenulink().click();
	Thread.sleep(1000);
	bp.getshoeslnk().click();
	Thread.sleep(2000);
	
	ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_10 Passed");
	
}

@DDDataProvider(datafile = "testdata\\testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "TC1", runmode = "Yes")
@Ignore
@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)

public void TC_DD_Baby_11(Hashtable<String, String> datatable) throws Exception{
	//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
	ExtentTestManager.getTest().log(Status.INFO, " verify babysubmnu_Toyslnk testcase");
	Thread.sleep(1000);
	
	//babypage bp = new babypage(driver);
	//bp.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));		
	//VideoRecorder_utlity.stopRecord();//End point of video recording
	
	
	//creating object for babypage.java
	
	babypage bp = new babypage(driver);
	
	//tc1 click main --> baby then click baby
	
	bp.getmenubtn().click();
	Thread.sleep(1000);
	bp.getbabymenulink().click();
	Thread.sleep(1000);
	bp.gettoyslnk().click();
	Thread.sleep(2000);
	bp.gettoysbackbtn().click();
	Thread.sleep(2000);
	bp.getbackbtn().click();
	ExtentTestManager.getTest().log(Status.INFO, "TC_DD_baby_11 Passed");
	                         
	
}

}


